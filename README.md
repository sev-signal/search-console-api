# Google Search API

## Google access configuration

* **Create a project**: [https://console.developers.google.com](https://console.developers.google.com)
* **Activate** the Google Search Console API and the google drive API for this project
* **Create a Service account** with a project owner role, get the json file
* **Authorize** the created email adress to access the site on the webmaster console (Dashboard > Settings > Users and Property Owners), restricted rights seem to be enough.


## Install the project

* clone the project
* launch shore-site start
* launch shore-start provision
* go to [http://localhost:8000/](http://localhost:8000/)

## Technical stack

* Nodejs + Mongo DB + [pm2](http://pm2.keymetrics.io/)
* Packages:
  * [Google API](https://github.com/google/google-api-nodejs-client)
  * Mongoose
  * Express
  * Mongodb


## Call samples
* http://localhost:8000/searchAnalytics/https%3A%2F%2Fwww.cellosignal.com/2017-08-01/2017-09-01
* http://localhost:8000/sitesList

## TODOLIST

  * send right HTTP code in routes
  * handle parameters (date, site...)
  * handle errors
  * write cronjobs
  * export from mongodb to spreadsheet
  
