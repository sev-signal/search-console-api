#!/bin/bash

# Retrieves last week's analytics for a list of hosts.
DAYS=7

START=$(date -v -${DAYS}d +%Y-%m-%d)
YESTERDAY=$(date -v -1d +%Y-%m-%d)
ENDPOINT="https://search-console.shore.localtest.me"

for host in "$@"; do
  curl -ksI "${ENDPOINT}/fetchAnalytics/https%3A%2F%2F${host}/${START}/${YESTERDAY}" > /dev/null
done
