#!/usr/bin/env bash

DIR="$( cd "$(dirname "$0")/../" ; pwd -P )"
WATCH="--watch"
WATCH=""

echo "Running node provisioning tasks"

(
     cd $DIR ; \
     npm install ; \
     pm2 start process.json
)
