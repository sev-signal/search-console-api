const express = require('express');
var mongoose = require('mongoose');
var config = require('./app/config');

const app            = express();
const port = 8000;


//connect to database
//var db = mongoose.createConnection(config.mongoUri);
mongoose.connect(config.mongoUri);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'unable to connect to database at ' + config.mongoUri));
db.once('open', function() {
    console.log('connected to mongodb ' + config.mongoUri);
});

//import routes
require('./app/models/searchData');
require('./app/routes')(app, {});

//start the server
app.listen(port, function() {
    console.log('Server ready on port ' + port);
});
