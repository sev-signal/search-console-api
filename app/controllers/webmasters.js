const google = require('googleapis');
const webmasters = google.webmasters('v3');
const key = require('../../signal-service-account.json');
const mongoose = require('mongoose');
const SearchData = mongoose.model('SearchData');

authorize = function(req, res, callback) {
    const jwtClient = new google.auth.JWT(
        key.client_email,
        null,
        key.private_key,
        // an array of auth scopes
        [
            'https://www.googleapis.com/auth/drive',
            'https://www.googleapis.com/auth/spreadsheets',
            'https://www.googleapis.com/auth/webmasters'
        ],
        null
    );

    jwtClient.authorize(function (err, tokens) {
        if (err) {
            console.error(err);
            res.status(403).send('Permission denied.');
            return;
        }

        callback(req, res, jwtClient);
    });
};

//Get the list of sites that the account can manage
exports.getSitesList = function(req, res) {
    authorize(req, res, function(req, res, auth) {
        webmasters.sites.list({auth: auth}, function (err, resp) {
            if (err) {
                console.error(err);
                return;
            }

            res.send(resp);
        });
    });
};

//get the data
exports.getSearchAnalytics = function(req, res) {
    authorize(req, res, function(req, res, auth) {
        //the URL needs to be encoded: https%3A%2F%2Fwww.cellosignal.com
        //date format: 2017-08-01
        var url = encodeURIComponent(req.params.url);
        var start = req.params.start;
        var end = req.params.end;

        var params = {
            auth: auth,
            siteUrl: url,
            resource : {
                startDate: start,
                endDate: end,
                dimensions:['query', 'page', 'date', 'device', 'country'],
                //rowLimit:10
            },
            aggregationType: 'byPage'
        };

        webmasters.searchanalytics.query(params, function (err, resp) {
            if (err) {
                console.error(err);
                return;
            }

            res.send(resp);
        });
    });
};


exports.saveDataToMongo = function(req, res) {
    authorize(req, res, function(req, res, auth) {
		var params = {
		    auth: auth,
		    siteUrl: encodeURIComponent('https://www.cellosignal.com'),
		    resource : {
		      startDate: '1970-01-01',
		      endDate: '2037-12-31',
		      dimensions:['query', 'page', 'date', 'device', 'country'],
		      //rowLimit:10
		    },
		    aggregationType: 'byPage'
		};

		webmasters.searchanalytics.query(params, function (err, resp) {

			resp.rows.forEach(function(element) {
			    var words = element.keys[0];
			    var url = element.keys[1];
			    var date = element.keys[2];
			    var device = element.keys[3];
			    var country = element.keys[4]
			    var clicks = element.clicks;
			    var impressions = element.impressions;
			    var ctr = element.ctr;
			    var position = element.position;

                //write into mongodb
                SearchData.create(
				    { "words": words, "url": url, "date": date, "device": device, "country": country, "clicks": clicks, "impressions": impressions, "ctr": ctr, "position": position }
				  , function (err) {
				    if (err) {
				    	return console.log(err);
                    }
				  });
			});
			res.send('data imported');
			if(err) {
			  	console.log(err);
			}
		});
	});
};

exports.getDataFromMongo = function(req, res) {
	SearchData.find(function (err, data) {
        if (err) return console.error(err);
        res.send(data);
    })

};
