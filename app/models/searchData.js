
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var SearchDataSchema = new Schema({
   words: String,
   url: String,
   date: Date,
   device: String,
   country: String,
   clicks: Number,
   impressions: Number,
   ctr: Number,
   position: Number
});

mongoose.model('SearchData', SearchDataSchema);
