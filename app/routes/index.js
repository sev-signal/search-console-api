const searchConsoleRoutes = require('./search_console_routes');
module.exports = function(app, db) {

	app.get('/', (req, res) => {
	  res.send('Google Search Console API')
	});
	searchConsoleRoutes(app, db);
};
