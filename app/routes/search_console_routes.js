module.exports = function(app, db) {
	var webmasters = require('../controllers/webmasters.js');

	app.get('/sitesList', webmasters.getSitesList);
	app.get('/searchAnalytics/:url/:start/:end', webmasters.getSearchAnalytics);
	app.get('/saveDataToMongo', webmasters.saveDataToMongo);
    app.get('/getDataFromMongo', webmasters.getDataFromMongo);
};