#!/bin/bash

# Commands to provision the site


DIR=/shore_site

docker exec -it search-console-app bash -c "sh $DIR/scripts/provision.sh"
