#!/bin/bash

# Commands to run after the containers start
DIR=/shore_site

echo "Creating directories"

docker exec -it search-console-app bash -c "cd $DIR ; mkdir -p cache log && chmod 776 cache log"
